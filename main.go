// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//Minor edits by Entropy 2019

package main

import(
  "io"
  "os"
  "fmt"
  "log"
  "flag"
  "time"
  "bufio"
  "bytes"
  "strings"
  "math/rand"
)

type Prefix []string

func (p Prefix) String() string {
  return strings.Join(p, " ")
}

func (p Prefix) Shift(word string) {
  copy(p, p[1:])
  p[len(p)-1] = word

}

type Chain struct {
  chain   map[string][]string
  prefixLen  int
}

func NewChain(prefixLen int) *Chain {
  return &Chain{make(map[string][]string), prefixLen}
}

func (c *Chain) Build(r io.Reader) {
  br := bufio.NewReader(r)
  p := make(Prefix, c.prefixLen)
  var count int
  //This is so we don't grab the garbage html header every time
  for {

    var s string
    _, err := fmt.Fscan(br, &s)

    if err != nil {
      break //we break instead of panicing as we've reached
            //an EOF
    }
    if count > 25 {
      key := p.String()
      c.chain[key] = append(c.chain[key], s)
      p.Shift(s)


    }
    count++

  }
}

func (c *Chain) Generate(n int) string {
  p := make(Prefix, c.prefixLen)
  var words []string
  for i := 0;i < n;i++ {
    choices := c.chain[p.String()]
    if len(choices) == 0 {
      break //same as above
    }
    next := choices[rand.Intn(len(choices))]
    words = append(words, next)
    p.Shift(next)
  }
  return strings.Join(words, " ")

}


func main() {
	var buffer bytes.Buffer
	log.New(&buffer, "babelsalmon", log.Llongfile)
  if len(os.Args) > 2 || len(os.Args) == 1{
    fmt.Println("\033[38:2:255:0:0mINCORRENT NUMBER OF ARGUMENTS\033[0m")
    fmt.Println("\033[38:2:0:255:0mUsage is `./babelsalmon library/TEXTFILE`\033[0m")
    os.Exit(1)
  }

  numWords := flag.Int("words", 75, "maximum number of words to print")
  prefixLen := flag.Int("prefix", 2, "prefix length in words")

  flag.Parse() //cl flags
  rand.Seed(time.Now().UnixNano()) //seeeds!
  var text string

  for len(text) < 75 {

    garbageFile, err := os.Open("library/"+os.Args[1])
    if err != nil {
      panic(err)
    }

    defer garbageFile.Close()
    c := NewChain(*prefixLen) //init chain
    c.Build(garbageFile)
    text = c.Generate(*numWords) //gen text
    log.Print("\033[38:2:178:50:150m"+text[:140]+"\033[0m")
  }



}
